package com.gradle.test.gradle_demo.controllers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public  class Hello {
    private String title;
    private String value;
}
